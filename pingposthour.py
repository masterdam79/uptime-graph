#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import json
import sys
import os
from influxdb import InfluxDBClient
import json
from datetime import datetime
import pytz
import argparse

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=True)
parser.add_argument('--date', type=str)
parser.add_argument('--hour', type=str)
parser.add_argument('--response', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    tz_string = config['DETAILS']['TIMEZONE']

# Some variables
tz = pytz.timezone(tz_string)

tz_now = datetime.now(tz)
offset = tz_now.utcoffset()
stroffset = str(offset)
shortstroffset = stroffset[:-3]
#tz_now = args.date + "T" + args.time + ".000000+01:00"

if not args.date:
    args.date = datetime.today().strftime('%Y-%m-%d')

if not args.hour:
    args.hour = datetime.today().strftime('%H')
#print(args.date)

# Create and select influx database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database('uptime')
client.get_list_database()
client.switch_database('uptime')

# Start ping command
response = int(args.response)
#print(args.response)
#and then check the response...
if response == int(1):
    #print(args.host, 'is up!')
    json_body = [
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":00.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":01.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":02.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":03.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":04.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":05.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":06.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":07.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":08.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":09.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":10.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":11.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":12.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":13.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":14.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":15.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":16.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":17.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":18.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":19.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":20.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":21.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":22.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":23.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":24.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":25.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":26.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":27.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":28.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":29.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":30.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":31.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":32.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":33.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":34.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":35.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":36.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":37.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":38.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":39.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":40.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":41.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":42.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":43.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":44.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":45.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":46.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":47.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":48.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":49.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":50.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":51.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":52.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":53.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":54.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":55.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":56.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":57.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":58.000000+0"+shortstroffset,"fields":{"up":1}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":59.000000+0"+shortstroffset,"fields":{"up":1}}
]
else:
    #print(args.host, 'is down!')
    json_body = [
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":00.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":01.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":02.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":03.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":04.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":05.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":06.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":07.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":08.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":09.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":10.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":11.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":12.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":13.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":14.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":15.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":16.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":17.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":18.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":19.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":20.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":21.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":22.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":23.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":24.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":25.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":26.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":27.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":28.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":29.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":30.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":31.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":32.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":33.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":34.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":35.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":36.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":37.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":38.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":39.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":40.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":41.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":42.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":43.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":44.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":45.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":46.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":47.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":48.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":49.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":50.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":51.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":52.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":53.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":54.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":55.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":56.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":57.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":58.000000+0"+shortstroffset,"fields":{"up":0}},
{"measurement":"tracking","tags":{"host":args.host},"time":args.date + "T"+args.hour+":59.000000+0"+shortstroffset,"fields":{"up":0}}
]

#jsonStr = json.dumps(json_body)
#print(json_body)
client.write_points(json_body)
