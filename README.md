# Uptime Graph

This script aims to ping a given host and send this to an influxdb host so that you can graph the uptime easily.

# Setup

1. Install ```influxdb``` using ```pip```
  - ``python3 -m pip install influxdb```
2. Rename ```config``` to ```config.cfg```
  - Adapt values in ```config.cfg``` to match your environment
