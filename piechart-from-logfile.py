import pandas as pd
import matplotlib.pyplot as plt
import re
import argparse

# List of strings to ignore
ignore_list = ["Calculator", "Downloads", "Chrome", "Konsole", "RICE", "Natasari", "Reijmers", "Telegram", "Grafana", "Geary", "chrome", "code.Code", "matplotlib", "Nautilus", "eog.Eog", "__tk__messagebox.Dialog", "__tk_filedialog.TkFDialog", "gjs.Gjs", "gnome", "VSCodium", "Nextcloud", "KeePass", "konsole", "codium", "kitty", "whalebird", "simple-scan", "Rabobank", "iDeal", "tootctl", "BTC", "Not Found", "bitcoin", "tradingview", "TradingView", "New tab", "Untitled", "LinkedIn", "Speedtest", "VSCodium", "Navigator.Tor Browser"]

# Function to parse each line
def parse_line(line):
    if any(keyword in line for keyword in ignore_list):
        return None
    match = re.match(r"(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}CEST)\s+(\S+)\s+(\S+)\s+(.+)", line)
    if match:
        datetime = match.group(1)
        application = match.group(2)
        system = match.group(3)
        activity = match.group(4)
        if application and system and activity:  # Ensure all required fields are present
            return datetime, application, system, activity
    return None

# Function to logically group activities
def categorize_activity(activity):
    if any(keyword in activity for keyword in ["PRODMGT", "CMC-Intern-Product Management", "Product Management", "CMC Algemeen - Confluence", "Standaard Prijzenblad", "Producten en Diensten Portfolio", "PDC Items", "CMC Dienst", "PDP Activiteiten", "PDP Items", "PDIVN-", "NS-Tactischoverleg", "HAHP", "Service Portfolio Management", "Service Portfolio Board", "Chat | Claire Vreeburg | Microsoft Teams", "Consortium Roadmap", "Pre Pi CPO", "EDR op Openshift", "Palo Alto", "Cortex XDR Agent", "EDR critical path", "Martijn van der Vaart", "Herinrichting Confluence", "Ani Tatlidil", "Vragenlijst gebruik Confluence", "palo alto cortex xdr", "Search Images", "Dynatrace", "Developer Hub", "ovnkubernetes", "palo-alto", "cortex", "Roadmap communicatie", "stakeholder", "Nathalie", "Service portfolio board", "Wieteke Idema", "Confluence herinrichting", "Critical Insights", "Managed Monitoring", "herinrichting Confluence"]):
        return "017884 - 3 - 4602: Product Management & Consultancy"
    elif any(keyword in activity for keyword in ["OCP", "CEAP", "RHACS", "RHACM", "Container", "NIS2", "nis2", "ODF", "Developer Experience", "spaces/CS", "OpenShift", "Hybrid Cloud Console", "Quay", "GRC-", "zwakenberg", "bergfeld", "Backstage", "Obsidian", "Developerhub", "Ceph", "GT-C", "Refinement/planning", "Robert Mestrum", "refinment/planning", "Rick Stiphout", "Virtueel Development Cluster Azure", "rhacm", "MCP Refinement", "Dennis van der Meulen", "Vlad Mihai", "LCM Trajecten", "Thomas Venieris", "Edwin de Groot", "Thomas Venieris", "Theo Wilde", "SRE Meeting", "Roy van der Linden", "IPCC managers", "Procesmanagement", "Emy Manuela", "Organogram", "Stefan van Oostrum", "Sprint Planning", "Gerard Gerrist", "Peter Barendregt", "Azure Carbon Optimization", "kubernetes", "Standard Change Template", "Parabol", "Max Hoeven"]):
        return "017884 - 2 - 4602: Application Development Platform"
    elif any(keyword in activity for keyword in ["InSite", "AFAS", "afasinsite"]):
        return "Uren Registratie"
    elif any(keyword in activity for keyword in [".docx", ".xlsx", "pptx", "Microsoft 365", "office.com", "Microsoft Loop", "Power Automate", "login.microsoftonline.com", "wordonlinebusiness", "Microsoft Word", "Microsoft Lists", "conclusionfutureit-my.sharepoint.com", "Microsoft Forms", "Agenda | Microsoft Teams", "Chat | Microsoft Teams"]):
        return "Office 365"
    elif any(keyword in activity for keyword in ["Red Hat", "RedHat", "redhat"]):
        return "Red Hat"
    elif any(keyword in activity for keyword in ["Backlog", "Agile Board", "Issue navigator", "Plans - Dagelijkse mededelingen", "Richard's Plan", "Projects - Dagelijkse mededelingen", "Filters - Dagelijkse mededelingen", "Bulk Operation"]):
        return "Backlog Mgt"
    elif any(keyword in activity for keyword in ["CMC Intern", "SYD", "Viva Engage", "Start Your Day", "Conclusion Mission Critical | 24/7 mission critical IT", "Virtual Sciences Conclusion de data integratiespecialist", "Coworks", "Conclusion Xforce | The power of Open Source", "Virtual Sciences Conclusion is dé data integratiespecialist", "CoWorks", "ditr", "CONCLUSION - BUSINESS DONE DIFFERENTLY", "Tweewekelijkse businessupdate"]):
        return "Intranet & Interne Updates"
    elif any(keyword in activity for keyword in ["Report"]):
        return "Reports"
    elif any(keyword in activity for keyword in ["Product", "Scrum", "Maandelijks bijpraten PO's", "Evidence-Based Management", "MYR", "Luuk Lemmens", "Bob van Doorenmalen", "Sparren", "Doelstellingsgesprek Richard", "Geconcretiseerde doelen", "CMC PO's"]):
        return "PO 2.0"
    elif any(keyword in activity for keyword in ["kwartaalbijeenkomst", "Kwartaalbijeenkomst", "Kwartaal meeting", "Bila"]):
        return "Meetings"
    elif any(keyword in activity for keyword in ["Outlook", "Calendar | Microsoft Teams", "Teams en kanalen"]):
        return "Outlook/Teams"
    else:
        return "Other"

def main(file_path, work_hours):
    # Read the file content
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Parse the lines
    data = [parse_line(line) for line in lines if parse_line(line)]
    df = pd.DataFrame(data, columns=['datetime', 'application', 'system', 'activity'])

    # Categorize activities
    df['category'] = df['activity'].apply(categorize_activity)

    # Exclude "Other" category
    df_filtered = df[df['category'] != 'Other']

    # Group by category and count occurrences
    category_summary = df_filtered['category'].value_counts().reset_index()
    category_summary.columns = ['category', 'count']

    # Calculate total count
    total_count = category_summary['count'].sum()

    # Calculate time spent in each category based on specified work hours
    category_summary['time_hours'] = (category_summary['count'] / total_count) * work_hours
    category_summary['percentage'] = (category_summary['time_hours'] / work_hours) * 100

    # Generate pie chart
    plt.figure(figsize=(10, 6))
    plt.pie(category_summary['count'], labels=category_summary['category'], autopct='%1.1f%%', startangle=140)
    plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title('Activities Distribution')
    plt.show()

    # Output the time spent in each category
    print(f"\nTime spent in each category (in hours and percentage)(assuming {work_hours} hours):")
    for _, row in category_summary.iterrows():
        print(f"{row['category']}: {row['time_hours']:.2f} hours")

    # Output the time spent in each category
    print(f"\nTime spent in each category (in hours and percentage)(assuming {work_hours} hours):")
    for _, row in category_summary.iterrows():
        print(f"{row['category']}: {row['percentage']:.1f}%")

    # Output the log lines categorized as "Other"
    other_logs = df[df['category'] == 'Other']
    if not other_logs.empty:
        print("\nLog lines categorized as 'Other':")
        for _, row in other_logs.iterrows():
            print(f"{row['datetime']} {row['application']} {row['system']} {row['activity']}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate a pie chart from log file')
    parser.add_argument('file_path', type=str, help='The path to the log file')
    parser.add_argument('--work_hours', type=int, default=8, help='The number of work hours to assume (default is 8)')
    args = parser.parse_args()
    main(args.file_path, args.work_hours)
