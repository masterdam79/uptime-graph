import argparse
import requests
from ics import Calendar
from datetime import datetime

def fetch_ics_file(url):
    response = requests.get(url)
    response.raise_for_status()  # Raise an exception for HTTP errors
    return response.text

def list_calendar_items(ics_content, date_str):
    calendar = Calendar(ics_content)
    date = datetime.strptime(date_str, "%Y-%m-%d").date()
    
    events = [event for event in calendar.events if event.begin.date() == date]
    
    if not events:
        print(f"No events found for {date_str}.")
    else:
        print(f"Events for {date_str}:")
        for event in events:
            print(f"- {event.name} from {event.begin.time()} to {event.end.time()}")

def main():
    parser = argparse.ArgumentParser(description="List calendar items from an .ics file for a given day.")
    parser.add_argument("url", type=str, help="URL of the .ics file")
    parser.add_argument("date", type=str, help="Date in the format YYYY-MM-DD")
    
    args = parser.parse_args()
    
    ics_content = fetch_ics_file(args.url)
    list_calendar_items(ics_content, args.date)

if __name__ == "__main__":
    main()
