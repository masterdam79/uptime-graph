#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import json
import sys
import os
from influxdb import InfluxDBClient
import json
from datetime import datetime
import pytz
import argparse

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=True)
parser.add_argument('--company', type=str, required=False, help='Optional company name')
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    tz_string = config['DETAILS']['TIMEZONE']

# Some variables
tz = pytz.timezone(tz_string)
tz_now = datetime.now(tz)
offset = tz_now.utcoffset()
stroffset = str(offset)
shortstroffset = stroffset[:-3]

print(tz)
time_now = datetime.time(tz_now)
date_now = datetime.date(tz_now)
# 2021-10-06 15:47:40.106754+01:00
datetime_now = str(date_now) + " " + str(time_now.hour) + ":" + str(time_now.minute) + ":00.000000+0"+shortstroffset
tz_now_rfc3339 = tz_now.isoformat('T') + "Z"
print(tz_now)
print(datetime_now)
#sys.exit()

# Create and select influx database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database('uptime')
client.get_list_database()
client.switch_database('uptime')

# Start ping command
response = os.system("arping -c 1 " + args.host)

# Build the tags dictionary dynamically based on arguments
tags = {
    "host": args.host
}
if args.company:
    tags["company"] = args.company

#and then check the response...
if response == 0:
    print(args.host, 'is up!')
    json_body = [
    {
        "measurement": "tracking",
        "tags": tags,
        "time": datetime_now,
        "fields": {
            "up": 1
        }
    }
]
else:
    print(args.host, 'is down!')
    json_body = [
    {
        "measurement": "tracking",
        "tags": tags,
        "time": datetime_now,
        "fields": {
            "up": 0
        }
    }
]

jsonStr = json.dumps(json_body)
print(jsonStr)
client.write_points(json_body)
