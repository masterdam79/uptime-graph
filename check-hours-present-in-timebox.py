#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
from datetime import datetime, timedelta
import pytz
import argparse

parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=True)
parser.add_argument('--date', type=str)
parser.add_argument('--start_time', type=str, required=True)
parser.add_argument('--end_time', type=str)
parser.add_argument('--response', type=str, required=True)
args = parser.parse_args()

if not args.date:
    args.date = datetime.today().strftime('%Y-%m-%d')

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    exit(1)

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    tz_string = config['DETAILS']['TIMEZONE']

# Some variables
tz = pytz.timezone(tz_string)
tz_now = datetime.now(tz)
offset = tz_now.utcoffset()

# Create and select InfluxDB database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database('uptime')
client.get_list_database()
client.switch_database('uptime')

def calculate_end_time(start_time, end_time):
    if end_time:
        return end_time
    else:
        start_dt = datetime.strptime(start_time, '%H:%M')
        end_dt = start_dt + timedelta(minutes=10)
        return end_dt.strftime('%H:%M')

def check_responses(host, start_time, end_time, response_value, date, tz_offset):
    # Convert local time to UTC
    start_datetime_local = f'{date}T{start_time}:00'
    end_datetime_local = f'{date}T{end_time}:00' if end_time else None
    start_datetime_local = datetime.strptime(start_datetime_local, '%Y-%m-%dT%H:%M:%S')
    end_datetime_local = datetime.strptime(end_datetime_local, '%Y-%m-%dT%H:%M:%S') if end_time else None

    start_datetime_utc = start_datetime_local - tz_offset
    end_datetime_utc = end_datetime_local - tz_offset if end_time else None

    start_datetime_utc_str = start_datetime_utc.strftime('%Y-%m-%dT%H:%M:%SZ')
    end_datetime_utc_str = end_datetime_utc.strftime('%Y-%m-%dT%H:%M:%SZ') if end_time else None

    query = f'SELECT * FROM "tracking" WHERE "host" = \'{host}\' AND time >= \'{start_datetime_utc_str}\' AND time <= \'{end_datetime_utc_str}\' AND "up" = {response_value}'
    
    result = list(client.query(query).get_points())
    return bool(result)

if __name__ == "__main__":
    end_time = calculate_end_time(args.start_time, args.end_time)

    # Return True if there are any responses, otherwise False
    result = check_responses(args.host, args.start_time, end_time, args.response, args.date, offset)
    exit(0 if result else 1)
