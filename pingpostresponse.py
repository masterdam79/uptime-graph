#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import json
import sys
import os
from influxdb import InfluxDBClient
import json
from datetime import datetime
import pytz
import argparse

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=True)
parser.add_argument('--response', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    tz_string = config['DETAILS']['TIMEZONE']

# Some variables
#tz = pytz.timezone(tz_string)
#tz_now = datetime.now(tz)
date_now = datetime.today().strftime('%Y-%m-%d')
time_now = datetime.today().strftime('%H:%M')
tz_now = date_now + "T" + time_now + ".000000+01:00"
#tz_now_rfc3339 = tz_now.isoformat('T') + "Z"
#print(date_now)
#print(time_now)
print(tz_now)
#print(tz_now_rfc3339)
#sys.exit()

# Create and select influx database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database('uptime')
client.get_list_database()
client.switch_database('uptime')

# Start ping command
response = args.response
print(response)
#and then check the response...
if response == "1":
    print(args.host, 'is up!')
    json_body = [
    {
        "measurement": "tracking",
        "tags": {
            "host": args.host
        },
        "time": tz_now,
        "fields": {
            "up": 1
        }
    }
]
else:
    print(args.host, 'is down!')
    json_body = [
    {
        "measurement": "tracking",
        "tags": {
            "host": args.host
        },
        "time": tz_now,
        "fields": {
            "up": 0
        }
    }
]

jsonStr = json.dumps(json_body)
print(jsonStr)
client.write_points(json_body)
