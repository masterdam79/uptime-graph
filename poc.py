#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import json
import sys
import os
from datetime import datetime, timedelta
import pytz
import argparse

parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=True)
parser.add_argument('--date', type=str)
parser.add_argument('--start_time', type=str, required=True)
parser.add_argument('--end_time', type=str)
parser.add_argument('--response', type=str, required=True)
parser.add_argument('--company', type=str, help='Company name (optional)')
parser.add_argument('--write_to_influxdb', action='store_const', const=1, default=0)


args = parser.parse_args()

if not args.date:
    args.date = datetime.today().strftime('%Y-%m-%d')

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    database = config['DETAILS']['DATABASE']
    measurement = config['DETAILS']['MEASUREMENT']
    tz_string = config['DETAILS']['TIMEZONE']
    default_company = config['DETAILS']['COMPANY']

# Use the provided company name if given, otherwise use the default from the config file
company = args.company if args.company else default_company

# Some variables
tz = pytz.timezone(tz_string)
tz_now = datetime.now(tz)
offset = tz_now.utcoffset()
stroffset = str(offset)
shortstroffset = stroffset[:-3]

# Create and select influx database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(database)
client.get_list_database()
client.switch_database(database)

def calculate_end_time(start_time, end_time):
    if end_time:
        return end_time
    else:
        start_dt = datetime.strptime(start_time, '%H:%M')
        end_dt = start_dt + timedelta(minutes=10)
        return end_dt.strftime('%H:%M')

def iterate_over_time(start_time, end_time):
    start_hour, start_minute = map(int, start_time.split(':'))
    end_hour, end_minute = map(int, end_time.split(':'))

    current_hour, current_minute = start_hour, start_minute
    time_list = []
    while current_hour < end_hour or (current_hour == end_hour and current_minute <= end_minute):
        time_dict = {
            "measurement": measurement,
            "tags": {
                "host": args.host,
                "company": company
            },
            "time": args.date + "T" + str(current_hour) + ":" + str(current_minute) + ".000000+0" + shortstroffset,
            "fields": {
                "up": int(args.response)
            }
        }
        time_list.append(time_dict)
        current_minute += 1
        if current_minute == 60:
            current_hour += 1
            current_minute = 0

    return time_list

end_time = calculate_end_time(args.start_time, args.end_time)
json_output = iterate_over_time(args.start_time, end_time)
formatted_output = json.dumps(json_output, indent=4)

if args.write_to_influxdb:
    print("Writing to Influxdb")
    client.write_points(json_output)
